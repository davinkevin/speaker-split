export interface Timing {
  minutes: number;
  seconds: number;
}

export interface SplitWithDuration {
  duration: Timing;
  title: string;
}

export interface Format {
  id: string;
  duration: string;
  splits: Array<SplitWithDuration>;
}

export interface Presentation {
  id: string;
  title: string;
  formats: Array<Format>;
}

const devOpsFromPhilosophyToAction45: Format = {
  id: "45m",
  duration: "45m",
  splits: [
    {title: "Introduction", duration: {minutes: 2, seconds: 35}},
    {title: "Agility to DevOps", duration: {minutes: 1, seconds: 5}},
    {title: "DevOps is not equal to", duration: {minutes: 1, seconds: 45}},
    {title: "Reducing Silos", duration: {minutes: 8, seconds: 16}},
    {title: "Implements gradual change", duration: {minutes: 2, seconds: 44}},
    {title: "Accept failure as normal behavior", duration: {minutes: 2, seconds: 38}},
    {title: "Leverage tooling & automation", duration: {minutes: 14, seconds: 50}},
    {title: "Measure everything", duration: {minutes: 4, seconds: 7}},
    {title: "Sec, Leg and Fin", duration: {minutes: 4, seconds: 50}},
    {title: "Myself", duration: {minutes: 2, seconds: 10}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const dockerKubIstio40m: Format = {
  id: "40m",
  duration: "40m",
  splits: [
    {title: "Introduction", duration: {minutes: 1, seconds: 15}},
    {title: "Docker", duration: {minutes: 1, seconds: 1}},
    {title: "Docker Images", duration: {minutes: 3, seconds: 10}},
    {title: "Distroless", duration: {minutes: 1, seconds: 44}},
    {title: "Docker Usage", duration: {minutes: 2, seconds: 5}},
    {title: "Docker Best Practice", duration: {minutes: 0, seconds: 45}},
    {title: "Kubernetes", duration: {minutes: 2, seconds: 13}},
    {title: "kubectl", duration: {minutes: 4, seconds: 41}},
    {title: "logs", duration: {minutes: 1, seconds: 6}},
    {title: "network", duration: {minutes: 2, seconds: 12}},
    {title: "kustomize", duration: {minutes: 3, seconds: 16}},
    {title: "kub best practice", duration: {minutes: 3, seconds: 10}},
    {title: "Istio", duration: {minutes: 6, seconds: 45}},
    {title: "Istio metering", duration: {minutes: 1, seconds: 27}},
    {title: "Istio 404", duration: {minutes: 1, seconds: 10}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const istioKubernetesBestFriend47: Format = {
  id: "50m",
  duration: "50m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 20}},
    {title: "Our world is changing", duration: {minutes: 2, seconds: 15}},
    {title: "Presentation", duration: {minutes: 1, seconds: 7}},
    {title: "Buzzword bingo", duration: {minutes: 0, seconds: 47}},
    {title: "Istio History", duration: {minutes: 2, seconds: 30}},
    {title: "Main Goal of Istio", duration: {minutes: 2, seconds: 32}},
    {title: "Istio Architecture", duration: {minutes: 5, seconds: 27}},
    {title: "Observability", duration: {minutes: 4, seconds: 8}},
    {title: "Connect", duration: {minutes: 5, seconds: 57}},
    {title: "Control", duration: {minutes: 7, seconds: 13}},
    {title: "Be resilient all the time", duration: {minutes: 3, seconds: 17}},
    {title: "Having control with Fun", duration: {minutes: 1, seconds: 28}},
    {title: "Security", duration: {minutes: 3, seconds: 38}},
    {title: "Go beyond", duration: {minutes: 2, seconds: 21}},
    {title: "How to start", duration: {minutes: 2, seconds: 36}},
    {title: "Anthos", duration: {minutes: 0, seconds: 46}},
    {title: "Thank you", duration: {minutes: 0, seconds: 38}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const istioKubernetesBestFriend42: Format = {
  id: "45m",
  duration: "45m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 17}},
    {title: "Our world is changing", duration: {minutes: 2, seconds: 1}},
    {title: "Presentation", duration: {minutes: 1, seconds: 0}},
    {title: "Buzzword bingo", duration: {minutes: 0, seconds: 42}},
    {title: "Istio History", duration: {minutes: 2, seconds: 14}},
    {title: "Main Goal of Istio", duration: {minutes: 2, seconds: 16}},
    {title: "Istio Architecture", duration: {minutes: 4, seconds: 52}},
    {title: "Observability", duration: {minutes: 3, seconds: 42}},
    {title: "Connect", duration: {minutes: 5, seconds: 19}},
    {title: "Control", duration: {minutes: 6, seconds: 27}},
    {title: "Be resilient all the time", duration: {minutes: 2, seconds: 56}},
    {title: "Having control with Fun", duration: {minutes: 1, seconds: 18}},
    {title: "Security", duration: {minutes: 3, seconds: 15}},
    {title: "Go beyond", duration: {minutes: 2, seconds: 6}},
    {title: "How to start", duration: {minutes: 2, seconds: 19}},
    {title: "Anthos", duration: {minutes: 0, seconds: 42}},
    {title: "Thank you", duration: {minutes: 0, seconds: 34}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const istioKubernetesBestFriendV246: Format = {
    id: "v2-46m",
    duration: "46m",
    splits: [
      {title: "Introduction", duration: {minutes: 0, seconds: 17}},
      {title: "Introduction", duration: {minutes: 0, seconds: 54}},
      {title: "Our world is changing", duration: {minutes: 1, seconds: 44}},
      {title: "Presentation", duration: {minutes: 0, seconds: 45}},
      {title: "Buzzword bingo", duration: {minutes: 1, seconds: 49}},
      {title: "Istio History", duration: {minutes: 1, seconds: 28}},
      {title: "Main Goal of Istio", duration: {minutes: 4, seconds: 10}},
      {title: "Istio Architecture", duration: {minutes: 2, seconds: 32}},
      {title: "Observability", duration: {minutes: 3, seconds: 1}},
      {title: "Connect", duration: {minutes: 10, seconds: 6}},
      {title: "Control", duration: {minutes: 4, seconds: 41}},
      {title: "Be resilient all the time", duration: {minutes: 1, seconds: 43}},
      {title: "Having control with Fun", duration: {minutes: 3, seconds: 52}},
      {title: "Security", duration: {minutes: 4, seconds: 28}},
      {title: "A bit more…", duration: {minutes: 3, seconds: 45}},
      {title: "How to start", duration: {minutes: 0, seconds: 15}},
      {title: "Thank you", duration: {minutes: 0, seconds: 15}},
      {title: "End", duration: {minutes: 0, seconds: 0}}
    ]
  }
;


const jooqJoyOfSQL40: Format = {
  id: "40m",
  duration: "40m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 6}},
    {title: "Introduction", duration: {minutes: 1, seconds: 34}},
    {title: "Road to SQL", duration: {minutes: 3, seconds: 50}},
    {title: "JDBC", duration: {minutes: 1, seconds: 4}},
    {title: "EJB | ORM", duration: {minutes: 1, seconds: 10}},
    {title: "JPA", duration: {minutes: 5, seconds: 37}},
    {title: "Back to SQL", duration: {minutes: 8, seconds: 17}},
    {title: "Custom Syntax", duration: {minutes: 2, seconds: 48}},
    {title: "AR & DAO", duration: {minutes: 2, seconds: 22}},
    {title: "Back to SQL again", duration: {minutes: 6, seconds: 55}},
    {title: "SQL Over !", duration: {minutes: 2, seconds: 32}},
    {title: "Postgresql JSONB", duration: {minutes: 1, seconds: 39}},
    {title: "SQL Everywhere", duration: {minutes: 1, seconds: 27}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const jooqJoyOfSQL45: Format = {
  id: "45m",
  duration: "45m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 8}},
    {title: "Introduction", duration: {minutes: 1, seconds: 46}},
    {title: "Road to SQL", duration: {minutes: 4, seconds: 17}},
    {title: "JDBC", duration: {minutes: 1, seconds: 12}},
    {title: "EJB | ORM", duration: {minutes: 1, seconds: 18}},
    {title: "JPA", duration: {minutes: 6, seconds: 18}},
    {title: "Back to SQL", duration: {minutes: 9, seconds: 16}},
    {title: "Custom Syntax", duration: {minutes: 3, seconds: 9}},
    {title: "AR & DAO", duration: {minutes: 2, seconds: 39}},
    {title: "Back to SQL again", duration: {minutes: 7, seconds: 46}},
    {title: "SQL Over !", duration: {minutes: 2, seconds: 50}},
    {title: "Postgresql JSONB", duration: {minutes: 1, seconds: 51}},
    {title: "SQL Everywhere", duration: {minutes: 1, seconds: 32}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const jooqJoyOfSQL50: Format = {
  id: "50m",
  duration: "50m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 8}},
    {title: "Introduction", duration: {minutes: 1, seconds: 55}},
    {title: "Road to SQL", duration: {minutes: 4, seconds: 40}},
    {title: "JDBC", duration: {minutes: 1, seconds: 18}},
    {title: "EJB | ORM", duration: {minutes: 1, seconds: 25}},
    {title: "JPA", duration: {minutes: 6, seconds: 51}},
    {title: "Back to SQL", duration: {minutes: 10, seconds: 6}},
    {title: "Custom Syntax", duration: {minutes: 3, seconds: 25}},
    {title: "AR & DAO", duration: {minutes: 2, seconds: 53}},
    {title: "Back to SQL again", duration: {minutes: 8, seconds: 27}},
    {title: "SQL Over !", duration: {minutes: 3, seconds: 5}},
    {title: "Postgresql JSONB", duration: {minutes: 2, seconds: 1}},
    {title: "SQL Everywhere", duration: {minutes: 1, seconds: 46}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const jooqJoyOfSQL75: Format = {
  id: "75m",
  duration: "75m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 13}},
    {title: "Introduction", duration: {minutes: 3, seconds: 0}},
    {title: "Road to SQL", duration: {minutes: 7, seconds: 17}},
    {title: "JDBC", duration: {minutes: 2, seconds: 3}},
    {title: "EJB | ORM", duration: {minutes: 2, seconds: 12}},
    {title: "JPA", duration: {minutes: 10, seconds: 42}},
    {title: "Back to SQL", duration: {minutes: 15, seconds: 46}},
    {title: "Custom Syntax", duration: {minutes: 5, seconds: 21}},
    {title: "AR & DAO", duration: {minutes: 4, seconds: 30}},
    {title: "Back to SQL again", duration: {minutes: 13, seconds: 12}},
    {title: "SQL Over !", duration: {minutes: 4, seconds: 50}},
    {title: "Postgresql JSONB", duration: {minutes: 3, seconds: 9}},
    {title: "SQL Everywhere", duration: {minutes: 2, seconds: 45}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const kustomize20: Format = {
  id: "20m",
  duration: "20m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 6}},
    {title: "Hello world", duration: {minutes: 0, seconds: 37}},
    {title: "Recipe push-pull", duration: {minutes: 0, seconds: 50}},
    {title: "Ingredients", duration: {minutes: 1, seconds: 47}},
    {title: "Tooling…", duration: {minutes: 1, seconds: 32}},
    {title: "kustomization.yaml", duration: {minutes: 6, seconds: 20}},
    {title: "Multi env", duration: {minutes: 5, seconds: 11}},
    {title: "Component", duration: {minutes: 1, seconds: 34}},
    {title: "Plugin", duration: {minutes: 1, seconds: 28}},
    {title: "Conclusion", duration: {minutes: 0, seconds: 35}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const kustomize25: Format = {
  id: "25m",
  duration: "25m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 8}},
    {title: "Hello world", duration: {minutes: 0, seconds: 44}},
    {title: "Recipe push-pull", duration: {minutes: 1, seconds: 0}},
    {title: "Ingredients", duration: {minutes: 2, seconds: 9}},
    {title: "Tooling…", duration: {minutes: 1, seconds: 50}},
    {title: "kustomization.yaml", duration: {minutes: 7, seconds: 36}},
    {title: "Multi env", duration: {minutes: 6, seconds: 13}},
    {title: "Component", duration: {minutes: 1, seconds: 53}},
    {title: "Plugin", duration: {minutes: 1, seconds: 45}},
    {title: "Conclusion", duration: {minutes: 0, seconds: 42}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const kustomize30: Format = {
  id: "30m",
  duration: "30m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 10}},
    {title: "Hello world", duration: {minutes: 0, seconds: 53}},
    {title: "Recipe push-pull", duration: {minutes: 1, seconds: 13}},
    {title: "Ingredients", duration: {minutes: 2, seconds: 36}},
    {title: "Tooling…", duration: {minutes: 2, seconds: 13}},
    {title: "kustomization.yaml", duration: {minutes: 9, seconds: 11}},
    {title: "Multi env", duration: {minutes: 7, seconds: 30}},
    {title: "Component", duration: {minutes: 2, seconds: 17}},
    {title: "Plugin", duration: {minutes: 2, seconds: 7}},
    {title: "Conclusion", duration: {minutes: 0, seconds: 50}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const kustomize33: Format = {
  id: "33m",
  duration: "33m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 11}},
    {title: "Hello world", duration: {minutes: 1, seconds: 0}},
    {title: "Recipe push-pull", duration: {minutes: 1, seconds: 24}},
    {title: "Ingredients", duration: {minutes: 2, seconds: 57}},
    {title: "Tooling…", duration: {minutes: 2, seconds: 32}},
    {title: "kustomization.yaml", duration: {minutes: 10, seconds: 28}},
    {title: "Multi env", duration: {minutes: 8, seconds: 33}},
    {title: "Component", duration: {minutes: 2, seconds: 36}},
    {title: "Plugin", duration: {minutes: 2, seconds: 25}},
    {title: "Conclusion", duration: {minutes: 0, seconds: 57}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const kustomize43: Format = {
  id: "43m",
  duration: "43m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 14}},
    {title: "Hello world", duration: {minutes: 1, seconds: 18}},
    {title: "Recipe push-pull", duration: {minutes: 1, seconds: 49}},
    {title: "Ingredients", duration: {minutes: 3, seconds: 50}},
    {title: "Tooling…", duration: {minutes: 3, seconds: 18}},
    {title: "kustomization.yaml", duration: {minutes: 13, seconds: 37}},
    {title: "Multi env", duration: {minutes: 11, seconds: 8}},
    {title: "Component", duration: {minutes: 3, seconds: 23}},
    {title: "Plugin", duration: {minutes: 3, seconds: 8}},
    {title: "Conclusion", duration: {minutes: 1, seconds: 15}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
}

const selectAmazingPG27: Format = {
  id: "43m",
  duration: "43m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 14}},
    {title: "Hello world", duration: {minutes: 1, seconds: 18}},
    {title: "Recipe push-pull", duration: {minutes: 1, seconds: 49}},
    {title: "Ingredients", duration: {minutes: 3, seconds: 50}},
    {title: "Tooling…", duration: {minutes: 3, seconds: 18}},
    {title: "kustomization.yaml", duration: {minutes: 13, seconds: 37}},
    {title: "Multi env", duration: {minutes: 11, seconds: 8}},
    {title: "Component", duration: {minutes: 3, seconds: 23}},
    {title: "Plugin", duration: {minutes: 3, seconds: 8}},
    {title: "Conclusion", duration: {minutes: 1, seconds: 15}},
    {title: "End", duration: {minutes: 0, seconds: 0}}
  ]
};

const selectAmazingPG30: Format = {
  id: "30m",
  duration: "30m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 17}},
    {title: "PG History", duration: {minutes: 1, seconds: 14}},
    {title: "Me Myself and I", duration: {minutes: 1, seconds: 12}},
    {title: "SQL", duration: {minutes: 1, seconds: 31}},
    {title: "Sakila", duration: {minutes: 0, seconds: 51}},
    {title: "CTE", duration: {minutes: 2, seconds: 29}},
    {title: "Window Functions", duration: {minutes: 0, seconds: 56}},
    {title: "Filter()", duration: {minutes: 1, seconds: 16}},
    {title: "row_number()", duration: {minutes: 2, seconds: 8}},
    {title: "Serial & UUID", duration: {minutes: 2, seconds: 47}},
    {title: "Constraints", duration: {minutes: 1, seconds: 38}},
    {title: "Type", duration: {minutes: 2, seconds: 13}},
    {title: "Listen & Notify", duration: {minutes: 1, seconds: 18}},
    {title: "ltree & more", duration: {minutes: 3, seconds: 43}},
    {title: "FDW", duration: {minutes: 2, seconds: 41}},
    {title: "Don't & do!", duration: {minutes: 3, seconds: 10}},
    {title: "Thank you!", duration: {minutes: 0, seconds: 0}}
  ]
};

const selectAmazingPG40: Format = {
    id: "40m",
    duration: "40m",
    splits: [
      {title: "Introduction", duration: {minutes: 0, seconds: 24}},
      {title: "PG History", duration: {minutes: 0, seconds: 58}},
      {title: "Me Myself and I", duration: {minutes: 1, seconds: 36}},
      {title: "SQL", duration: {minutes: 1, seconds: 32}},
      {title: "Sakila", duration: {minutes: 0, seconds: 21}},
      {title: "CTE", duration: {minutes: 2, seconds: 8}},
      {title: "Window Functions", duration: {minutes: 1, seconds: 3}},
      {title: "filter()", duration: {minutes: 1, seconds: 23}},
      {title: "row_number()", duration: {minutes: 1, seconds: 55}},
      {title: "join lateral", duration: {minutes: 1, seconds: 52}},
      {title: "Serial & UUID", duration: {minutes: 2, seconds: 58}},
      {title: "Constraints", duration: {minutes: 1, seconds: 42}},
      {title: "Type", duration: {minutes: 2, seconds: 16}},
      {title: "Listen & Notify", duration: {minutes: 1, seconds: 33}},
      {title: "ltree & more", duration: {minutes: 4, seconds: 18}},
      {title: "FDW", duration: {minutes: 5, seconds: 30}},
      {title: "Don't and do!", duration: {minutes: 4, seconds: 13}},
      {title: "Everywhere", duration: {minutes: 3, seconds: 18}},
      {title: "Thank you!", duration: {minutes: 0, seconds: 0}}
    ]
  };

const selectAmazingPG44: Format = {
  id: "44m",
  duration: "44m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 16}},
    {title: "PG History", duration: {minutes: 1, seconds: 11}},
    {title: "Me Myself and I", duration: {minutes: 1, seconds: 25}},
    {title: "SQL", duration: {minutes: 2, seconds: 3}},
    {title: "Sakila", duration: {minutes: 0, seconds: 52}},
    {title: "CTE", duration: {minutes: 2, seconds: 9}},
    {title: "Window Functions", duration: {minutes: 1, seconds: 18}},
    {title: "filter()", duration: {minutes: 1, seconds: 22}},
    {title: "lag() & lead()", duration: {minutes: 1, seconds: 43}},
    {title: "[dense_]rank()", duration: {minutes: 2, seconds: 2}},
    {title: "row_number()", duration: {minutes: 1, seconds: 50}},
    {title: "join lateral", duration: {minutes: 1, seconds: 42}},
    {title: "pagination", duration: {minutes: 1, seconds: 56}},
    {title: "Serial & UUID", duration: {minutes: 3, seconds: 28}},
    {title: "Constraints", duration: {minutes: 1, seconds: 23}},
    {title: "Type", duration: {minutes: 2, seconds: 10}},
    {title: "on conflict", duration: {minutes: 1, seconds: 19}},
    {title: "Merge", duration: {minutes: 2, seconds: 55}},
    {title: "Listen & Notify", duration: {minutes: 1, seconds: 34}},
    {title: "ltree & more", duration: {minutes: 3, seconds: 46}},
    {title: "FDW", duration: {minutes: 3, seconds: 22}},
    {title: "Don't and do!", duration: {minutes: 2, seconds: 33}},
    {title: "Everywhere", duration: {minutes: 1, seconds: 41}},
    {title: "Thank you!", duration: {minutes: 0, seconds: 0}}
  ]
};

const selectAmazingPG47: Format = {
  id: "47m",
  duration: "47m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 17}},
    {title: "PG History", duration: {minutes: 1, seconds: 16}},
    {title: "Me Myself and I", duration: {minutes: 1, seconds: 30}},
    {title: "SQL", duration: {minutes: 2, seconds: 10}},
    {title: "Sakila", duration: {minutes: 0, seconds: 55}},
    {title: "CTE", duration: {minutes: 2, seconds: 17}},
    {title: "Window Functions", duration: {minutes: 1, seconds: 22}},
    {title: "filter()", duration: {minutes: 1, seconds: 27}},
    {title: "lag() & lead()", duration: {minutes: 1, seconds: 50}},
    {title: "[dense_]rank()", duration: {minutes: 2, seconds: 9}},
    {title: "row_number()", duration: {minutes: 1, seconds: 56}},
    {title: "join lateral", duration: {minutes: 1, seconds: 48}},
    {title: "pagination", duration: {minutes: 2, seconds: 3}},
    {title: "Serial & UUID", duration: {minutes: 3, seconds: 41}},
    {title: "Constraints", duration: {minutes: 1, seconds: 28}},
    {title: "Type", duration: {minutes: 2, seconds: 17}},
    {title: "on conflict", duration: {minutes: 1, seconds: 24}},
    {title: "Merge", duration: {minutes: 3, seconds: 5}},
    {title: "Listen & Notify", duration: {minutes: 1, seconds: 40}},
    {title: "ltree & more", duration: {minutes: 3, seconds: 59}},
    {title: "FDW", duration: {minutes: 3, seconds: 34}},
    {title: "Don't and do!", duration: {minutes: 2, seconds: 42}},
    {title: "Everywhere", duration: {minutes: 1, seconds: 47}},
    {title: "Thank you!", duration: {minutes: 0, seconds: 0}}
  ]
};

const selectAmazingPG48: Format = {
  id: "48m",
  duration: "48m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 22}},
    {title: "PG History", duration: {minutes: 1, seconds: 41}},
    {title: "Me Myself and I", duration: {minutes: 1, seconds: 59}},
    {title: "SQL", duration: {minutes: 2, seconds: 52}},
    {title: "Sakila", duration: {minutes: 1, seconds: 13}},
    {title: "CTE", duration: {minutes: 3, seconds: 2}},
    {title: "Window Functions", duration: {minutes: 1, seconds: 49}},
    {title: "filter()", duration: {minutes: 1, seconds: 55}},
    {title: "row_number()", duration: {minutes: 2, seconds: 34}},
    {title: "join lateral", duration: {minutes: 2, seconds: 23}},
    {title: "Serial & UUID", duration: {minutes: 4, seconds: 53}},
    {title: "Constraints", duration: {minutes: 1, seconds: 57}},
    {title: "Type", duration: {minutes: 3, seconds: 2}},
    {title: "Listen & Notify", duration: {minutes: 2, seconds: 12}},
    {title: "ltree & more", duration: {minutes: 5, seconds: 17}},
    {title: "FDW", duration: {minutes: 4, seconds: 44}},
    {title: "Don't and do!", duration: {minutes: 3, seconds: 35}},
    {title: "Everywhere", duration: {minutes: 2, seconds: 22}},
    {title: "Thank you!", duration: {minutes: 0, seconds: 0}}
  ]
};

const gatewayAPI10Years45m: Format = {
  id: "45m",
  duration: "45m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 14}},
    {title: "Kubernetes 10 years", duration: {minutes: 0, seconds: 53}},
    {title: "Me Myself and I", duration: {minutes: 1, seconds: 38}},
    {title: "Network", duration: {minutes: 2, seconds: 6}},
    {title: "Kubernetes v1.0", duration: {minutes: 7, seconds: 36}},
    {title: "Alternative Ingresses", duration: {minutes: 3, seconds: 24}},
    {title: "GatewayAPI & Concepts", duration: {minutes: 7, seconds: 26}},
    {title: "CRDs", duration: {minutes: 5, seconds: 50}},
    {title: "Routes", duration: {minutes: 5, seconds: 10}},
    {title: "Deployment Pattern", duration: {minutes: 3, seconds: 5}},
    {title: "Distribution", duration: {minutes: 0, seconds: 58}},
    {title: "Advanced Routing", duration: {minutes: 3, seconds: 43}},
    {title: "How to?", duration: {minutes: 1, seconds: 57}},
    {title: "Ending", duration: {minutes: 0, seconds: 0}}
  ]
}

const gatewayAPI10Years40m: Format = {
  id: "40m",
  duration: "40m",
  splits: [
    {title: "Introduction", duration: {minutes: 0, seconds: 12}},
    {title: "Kubernetes 10 years", duration: {minutes: 0, seconds: 48}},
    {title: "Me Myself and I", duration: {minutes: 1, seconds: 29}},
    {title: "Network", duration: {minutes: 1, seconds: 54}},
    {title: "Kubernetes v1.0", duration: {minutes: 6, seconds: 54}},
    {title: "Alternative Ingresses", duration: {minutes: 3, seconds: 5}},
    {title: "GatewayAPI & Concepts", duration: {minutes: 6, seconds: 45}},
    {title: "CRDs", duration: {minutes: 5, seconds: 18}},
    {title: "Routes", duration: {minutes: 4, seconds: 41}},
    {title: "Deployment Pattern", duration: {minutes: 2, seconds: 48}},
    {title: "Distribution", duration: {minutes: 0, seconds: 52}},
    {title: "Advanced Routing", duration: {minutes: 3, seconds: 22}},
    {title: "How to?", duration: {minutes: 1, seconds: 46}},
    {title: "Ending", duration: {minutes: 0, seconds: 0}}
  ]
}

export const presentations: Array<Presentation> = [
  {
    id: 'devOpsFromPhilosophyToAction',
    title: 'DevOps, From Philosophy to Action',
    formats: [devOpsFromPhilosophyToAction45]
  },
  {
    id: 'dockerKubIstio',
    title: 'Docker, Kub & Istio',
    formats: [dockerKubIstio40m]
  },
  {
    id: 'istioKubernetesBestFriend',
    title: 'Istio, your Kubernetes best friend ❤️',
    formats: [istioKubernetesBestFriend42, istioKubernetesBestFriend47, istioKubernetesBestFriendV246]
  },
  {
    id: 'jooqJoyOfSQL',
    title: 'JOOQ, Joy of SQL',
    formats: [jooqJoyOfSQL40, jooqJoyOfSQL45, jooqJoyOfSQL50, jooqJoyOfSQL75]
  },
  {
    id: 'kustomize',
    title: `Let's kustomize our deployments with style 🤩!`,
    formats: [kustomize20, kustomize25, kustomize30, kustomize33, kustomize43]
  },
  {
    id: 'selectAmazingPG',
    title: `SELECT 'amazing_features' from "postgresql"`,
    formats: [selectAmazingPG27, selectAmazingPG30, selectAmazingPG40, selectAmazingPG44, selectAmazingPG47, selectAmazingPG48]
  },
  {
    id: 'gateway-api-10-years',
    title: `Gateway API, 10 years of maturation for a new Kubernetes network API`,
    formats: [gatewayAPI10Years40m, gatewayAPI10Years45m]
  },
];
