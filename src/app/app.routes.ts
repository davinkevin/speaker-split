import {Routes} from '@angular/router';



// export const routes: Routes = [];
export const routes: Routes = [
  {path: '', loadComponent: () => import('./presentations/presentations.component').then(m => m.PresentationsComponent)},
  {path: 'timer/:presentationId/:formatId', loadComponent: () => import('./timer/timer.component').then(m => m.TimerComponent)}
];
