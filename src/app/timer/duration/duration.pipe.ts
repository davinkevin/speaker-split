import { Pipe, PipeTransform, inject } from '@angular/core';
import {DecimalPipe} from '@angular/common';

@Pipe({
  standalone: true,
  name: 'duration',
})
export class DurationPipe implements PipeTransform {

  private decimalPipe = inject(DecimalPipe);

  decimalFormat = '2.0-0';

  transform(value: any, ..._args: any[]): any {
    const symbol = +value == 0 ? ''
      : +value > 0 ? '+'
      :'-';

    const duration = Math.abs(value);

    const seconds = duration % 60;
    const minutes = ( duration - seconds ) / 60;

    const secondsAsString = this.decimalPipe.transform(seconds, this.decimalFormat);
    const minutesAsString = this.decimalPipe.transform(minutes, this.decimalFormat);

    return `${symbol}${minutesAsString}:${secondsAsString}`;
  }

}
