
// Don't include introduction
// Each timing should report the second when the split STARTS
const original = {
    id: "40m",
    duration: "40m",
    splits: [
        {title: "PG History", timing: {minutes: 0, seconds: 24}},
        {title: "Me Myself and I", timing: {minutes: 1, seconds: 22}},
        {title: "SQL", timing: {minutes: 2, seconds: 58}},
        {title: "Sakila", timing: {minutes: 4, seconds: 30}},
        {title: "CTE", timing: {minutes: 4, seconds: 51}},
        {title: "Window Functions", timing: {minutes: 6, seconds: 59}},
        {title: "filter()", timing: {minutes: 8, seconds: 2}},
        {title: "row_number()", timing: {minutes: 9, seconds: 25}},
        {title: "join lateral", timing: {minutes: 11, seconds: 20}},
        {title: "Serial & UUID", timing: {minutes: 13, seconds: 12}},
        {title: "Constraints", timing: {minutes: 16, seconds: 10}},
        {title: "Type", timing: {minutes: 17, seconds: 52}},
        {title: "Listen & Notify", timing: {minutes: 20, seconds: 8}},
        {title: "ltree & more", timing: {minutes: 21, seconds: 41}},
        {title: "FDW", timing: {minutes: 25, seconds: 59}},
        {title: "Don't and do!", timing: {minutes: 31, seconds: 29}},
        {title: "Everywhere", timing: {minutes: 35, seconds: 42}},
        {title: "Thank you!", timing: {minutes: 39, seconds: 0}}
    ]
};
;

function diffBetweenSplits({current, next}) {
    if (next === undefined) {
        return {title: current.title, duration: {minutes: 0, seconds: 0}};
    }

    const currentTimeCodeInSec = current.timing.minutes * 60 + current.timing.seconds
    const nextTimeCodeInSec = next.timing.minutes * 60 + next.timing.seconds
    const duration = nextTimeCodeInSec - currentTimeCodeInSec

    return {title: current.title, duration: convertSecToTime(duration)}
}

function convertSecToTime(duration) {
    const seconds = duration % 60;
    const minutes = (duration - seconds) / 60;
    return {minutes, seconds}
}

const originalWithIntro = {
    ...original,
    splits: [
        {title: 'Introduction', timing: {minutes: 0, seconds: 0}},
        ...original.splits,
    ]
}

const splits = [...originalWithIntro.splits]
const splitsPlusOne = [...original.splits]

const newSplits = splits
    .map((current, i) => ({ current, next: splitsPlusOne[i]}))
    .map(diffBetweenSplits)
    // .forEach((v) => console.log(v))


// console.dir({...original, splits: newSplits})
console.log(JSON.stringify({...original, splits: newSplits}, null, 4))
// console.log({...original, splits: newSplits});

